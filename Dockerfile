FROM golang:alpine AS builder

RUN apk add --no-cache git

WORKDIR /serverApp

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -o server .

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /serverApp/server .
COPY --from=builder /serverApp/.env .
# Копируем файл .env в контейнер
CMD ["./server"]