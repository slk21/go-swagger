package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	httpSwagger "github.com/swaggo/http-swagger"
	"log"
	"net/http"
	"os"
	_ "studentgit.kata.academy/SLK/go-swagger/docs"
	handler "studentgit.kata.academy/SLK/go-swagger/handlers"
	prox "studentgit.kata.academy/SLK/go-swagger/proxy"
)

// @title Поиск данных адреса
// @version 1.0
// @description Данный сервис предоставляент возможность поиска информации об адресе, по его фактическом адресу или геолокации.
// @contact.name Вячеслав
// @contact.email slavalomov.4@gmail.com
// @host localhost:8080
// @BasePath /
// @accept json
// @produce json
func main() {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println(err)
		return

	}

	port := os.Getenv("SERVER_PORT")

	r := chi.NewRouter()

	r.Use(prox.Middlewares)
	r.Post("/api/address/search", handler.HandlerSearch)
	r.Post("/api/address/geocode", handler.HandlerGeo)
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:"+port+"/swagger/doc.json"), //The url pointing to API definition
	))

	log.Fatal(http.ListenAndServe(":"+port, r))

}
