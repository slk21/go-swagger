package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	handl "studentgit.kata.academy/SLK/go-swagger/handlers"
	model "studentgit.kata.academy/SLK/go-swagger/models"
	prox "studentgit.kata.academy/SLK/go-swagger/proxy"
	"testing"
)

func Test_HandlerGeo(t *testing.T) {
	geo := model.GeocodeRequest{
		Lat: 55.878,
		Lon: 37.653,
	}

	dataGeo, err := json.Marshal(geo)
	if err != nil {
		fmt.Println(err)
		return
	}

	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataGeo))
	req.Header.Set("contentType", "application/json")
	w := httptest.NewRecorder()
	handl.HandlerGeo(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, _ := io.ReadAll(res.Body)

	expectedStatusCode := "200 OK"
	if res.Status != expectedStatusCode {
		t.Errorf("Ожидаемый статус код: %s\nПолученный статус код: %s", expectedStatusCode, res.Status)
	}

	expectedData := "[{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"11\",\"geo_lat\":\"55.878315\",\"geo_lon\":\"37.65372\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"11А\",\"geo_lat\":\"55.878212\",\"geo_lon\":\"37.652016\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"13\",\"geo_lat\":\"55.878666\",\"geo_lon\":\"37.6524\"},{\"city\":\"Москва\",\"street\":\"Сухонская\",\"house\":\"9\",\"geo_lat\":\"55.877167\",\"geo_lon\":\"37.652481\"},{\"city\":\"Москва\",\"street\":\"\",\"house\":\"\",\"geo_lat\":\"55.75396\",\"geo_lon\":\"37.620393\"}]"
	if string(data) != expectedData {
		t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", expectedData, string(data))
	}
}

func Test_HandlerSearch(t *testing.T) {
	search := model.SearchRequest{
		Query: "москва сухонская 11",
	}

	dataSearch, err := json.Marshal(search)
	if err != nil {
		fmt.Println(err)
		return
	}

	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(dataSearch))
	req.Header.Set("contentType", "application/json")
	w := httptest.NewRecorder()
	handl.HandlerSearch(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, _ := io.ReadAll(res.Body)

	expectedStatusCode := "200 OK"
	if res.Status != expectedStatusCode {
		t.Errorf("Ожидаемый статус код: %s\nПолученный статус код: %s", expectedStatusCode, res.Status)
	}

	expectedData := "[{\"city\":\"\",\"street\":\"Сухонская\",\"house\":\"11\",\"geo_lat\":\"55.8782557\",\"geo_lon\":\"37.65372\"}]"
	if string(data) != expectedData {
		t.Errorf("Ожидаемый ответ: %s\nПолученный ответ: %s", expectedData, string(data))
	}
}

type testCase struct {
	url             string
	expectedCode    int
	expectedRequest string
}

func Test_middlewares(t *testing.T) {

	testsi := []testCase{
		{
			url:             "/api/address/search",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/api/address/geocode",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/",
			expectedCode:    400,
			expectedRequest: "400 :Неверный формат запроса",
		},
	}

	for _, val := range testsi {
		req := httptest.NewRequest(http.MethodGet, val.url, nil)

		rr := httptest.NewRecorder()

		hadler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte("TEST"))
			if err != nil {
				t.Fatal(err)
			}
		})

		prox.Middlewares(hadler).ServeHTTP(rr, req)

		if val.expectedCode != rr.Code {
			t.Errorf("Ожидал: %v \n Получил: %v", val.expectedCode, rr.Code)
		}

		if val.expectedRequest != rr.Body.String() {
			t.Errorf("Ожидал: %s \n Получил: %s", val.expectedRequest, rr.Body.String())
		}
	}

}
