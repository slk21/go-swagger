package models

// Структура запроса для поиска по фактическому адресу

type SearchRequest struct {
	Query string `json:"query"`
}

// Структура для хранения множества ответов от dadata.ru на запрос по поиску инофрмации о фактическом адресе

type SearchResponse struct {
	Addresses []Address `json:"addresses"`
}
