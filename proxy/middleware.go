package proxy

import (
	"fmt"
	"net/http"
	"strings"
)

func Middlewares(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/api/address/search" || r.URL.Path == "/api/address/geocode" {
			next.ServeHTTP(w, r)
			return
		} else if strings.Contains(r.URL.Path, "/swagger/") {
			next.ServeHTTP(w, r)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("400 :Неверный формат запроса"))
		if err != nil {
			fmt.Println(err)
			return
		}
	})
}
